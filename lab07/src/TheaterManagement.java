import java.util.ArrayList;

public class TheaterManagement {
	ArrayList<Seat> seats;
	
	public double buyTicket(int time,char c,int col){
		int low = ((int)c)-65;
		Seat seat = new Seat(time,low,col,DataSeatPrice.ticketPrices[low][col]);
		if(!(seats.contains(seat))){ seats.add(seat); return seat.getPrice();}
		return 0;
	}

}
