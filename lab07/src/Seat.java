public class Seat {
	private int row;
	private int col;
	private int time;
	private double price;
	
	public Seat(int row,int col,int time,double price){
		this.row = row;
		this.col = col;
		this.time = time;
		this.price = price ;
	}
	
	public boolean equals(Object other){
		if(other==null) return false;
		if(!(other instanceof Seat)) return false;
		Seat s = (Seat)other;
		if(this.row==s.row && this.col==s.col && this.time==s.time && this.getPrice()==s.getPrice()) return true;
		return false;
	}

	public double getPrice() {
		return price;
	}


}
